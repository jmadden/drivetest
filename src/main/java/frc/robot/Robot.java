/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import edu.wpi.first.networktables.*;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

//pneumatics
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;

import com.ctre.phoenix.motorcontrol.*;
import com.ctre.phoenix.motorcontrol.can.*;

import frc.robot.olibs.util.*;


/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {

  Compressor c = new Compressor(0);
  boolean enabled = c.enabled();

  //DoubleSolenoid shifter = new DoubleSolenoid(,);

  WPI_TalonFX x = new WPI_TalonFX(5);
  WPI_TalonFX y = new WPI_TalonFX(6);

  //Different CAN for testing left

//WPI_TalonFX x = new WPI_TalonFX(9);
//WPI_TalonFX y = new WPI_TalonFX(10);
  
  WPI_TalonFX z = new WPI_TalonFX(7);
  WPI_TalonFX a = new WPI_TalonFX(8);
 
  SpeedControllerGroup left = new SpeedControllerGroup(x, y);
  SpeedControllerGroup right = new SpeedControllerGroup(z, a);
  DifferentialDrive drive = new DifferentialDrive(left, right);
  DAController driver = new DAController(0);
 
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  NetworkTableInstance inst;
  private NetworkTable table;
  NetworkTableEntry falcon5Speed;
  NetworkTableEntry falcon6Speed;
  
  private final SendableChooser<String> m_chooser = new SendableChooser<>();

  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    inst = NetworkTableInstance.getDefault();
    table =  inst.getTable("FalconTable");
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);

    c.setClosedLoopControl(true);
    
    table.getEntry("5speed").setDouble(0);
    table.getEntry("6speed").setDouble(0);
    
    table.getEntry("7speed").setDouble(0);
    table.getEntry("8speed").setDouble(0);
   /* 
    //Shifting
    if(driver.getRawButton(DualAction.A)) {
        shifter.set(Value.kForward);
    } else if (driver.getRawButton(DualAction.B)) {
        shifter.set(Value.kReverse);
    }
*/
  }

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    enabled = true;

    x.set(ControlMode.PercentOutput, table.getEntry("5speed").getDouble(0));
    y.set(ControlMode.PercentOutput, table.getEntry("6speed").getDouble(0));
    
    z.set(ControlMode.PercentOutput, table.getEntry("7speed").getDouble(0));
    a.set(ControlMode.PercentOutput, table.getEntry("8speed").getDouble(0));
    
    drive.arcadeDrive(driver.getRawAxis(DualAction.LeftYAxis), driver.getRawAxis(DualAction.RightXAxis), true);
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);
  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      default:
        // Put default auto code here
        break;
    }
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
  }

  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
  }
}
